# 维基系统简明教程

沈禹

2018/11/30

## 为什么?!
* Word不好吗？
* 钉钉不好吗？
* 电子邮件不好吗？

1. 知识分散在各处，需要的时候不知道去哪里找: “我上个月写的x.y.z版文档放在哪里了？我的文档/D盘/F:\工作？”
2. 存储的格式千差万别，有的需要私有软件才能解码(比如：盗版Word)
3. 巨大的浪费

- 存储空间：二进制文件没有diff的概念，若要看到更改的历史，又要使用专有软件，且只有人类能看见，没有API
- 时间和精力：下班前给客户发了一个巨大的PPT，又改了一个产品名称需要重发，家里网络又不好，还得跟人解释上个版本请作废...

### 我不想学一个新工具!

假设，每次找一个文件需要花费5分钟，平均每周要阅读/修改文件20次，那么一年下来要浪费 52*20*5=5200 分钟,或 86 小时，相当于沿318国道从合肥(423km青阳)到拉萨(4670km)往返一趟

![tibet](./img/tibetan-plateau-thinkstock-032817.jpg)

## 简介
### 什么是维基

* 一个开放编辑的知识库
* 知识主要以结构化的文本形式保存，知识点之前可以用超链接相关联(类似于HTML，见下页)
* 随时随地可以查看和编辑，每一个常用知识点有一个恒定的链接，唯一的真相来源


### 饮水思源(Philology)
- "Wiki"是夏威夷语中"快"的意思，发明者Cunningham的灵感来源于火奴鲁鲁机场大巴（Wiki Wiki Shuttle）的别名．
- 设计初衷是对HTML的一个补充，侧重于纯文本形式的知识共享．
- 并不等于我们熟知的维基百科（Wikipedia），而是所有这一类软件和运行在这些软件之上的网站的统称，其中最成功的软件Mediawiki就是维基百科的基础．

### Wiki Wiki 大巴

![Wiki Wiki Shuttle](https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/HNL_Wiki_Wiki_Bus.jpg/320px-HNL_Wiki_Wiki_Bus.jpg)

### Confluence

- 我们的维基网站是用Confluence搭建的，与Mediawiki不同，是一个企业维基
- 虽然是通过网页访问，但是官方只支持以下浏览器
     - Internet Explorer 11
     - Microsoft Edge
     - Chrome
     - Firefox
     - Safari (Mac only)
     - https://confluence.atlassian.com/doc/supported-platforms-207488198.html



## 使用
### 注册
   管理员会为大家注册帐号，请前往您的公司邮箱(...@ruijii.com)查收邮件

### 主界面
![homepage](./img/homepage.png)
### 主界面解释
1. 随时点击这个按钮，回到主界面
2. 其他用户的更新，类似于朋友圈
3. 您可能并不想看到 *所有人* 的更新？？
4. 查看空间、其他人的主页，以及最有用的按钮： *创建* 文档, 请多多使用这个按钮
5. 关于您自己的一切
6. 更改密码、语言、个人信息，都在这里

### "我为什么不能更改 X 设置?"
与“维基”之名相符，Confluence上的一切都是可以“编辑”的，包括文章和设置，请注意寻找这样的按钮:
- ![edit](./img/edit.png)
- ![editp](./img/editp.png)
- ![edite](./img/edite.png)


### 其他有用文档
- [Confluence官方文档, 英文](https://confluence.atlassian.com/doc/confluence-server-documentation-135922.html)
- [非官方中文版](https://www.cwiki.us/pages/viewpage.action?pageId=32998578#app-switcher)

## 创作
### 创建文档
如果点击 创建 边的 ... , 显示的是内建的各种文档模板，请大胆试验
![templates](./img/templates.png)

### 空间

- 空间相当于顶层文件夹，为方便查阅，请尽量把新的文档建立在已有的空间里
- 每位注册用户都有自己的个人空间，请在这个空间里尽情发挥

## 搜索

### 基础
- 使用右上角搜索栏
- 同词根等效原则: 与搜索词条同词根的词语均会被命中
      - 例如: project 与 projecting 为等效搜索，包含 project , projecting , projects , projected 之文章都会出现在结果中
- 通配符(Wildcards)
      - ? : 一个任意字母
      - * : 一连串任意长度的任意字母
      - 注意：通配符不能出现在开头

### 精确词组查询
- 使用双引号搜索单词序列，即词条以指定的顺序出现在文本中，中间不可以有其他单词，例如 "create content"
- 同词根等效原则在这里也适用，如上面的搜索也可以搜到 "created content"

### 逻辑运算符
- AND / OR / - 可以用来组合与排除查询词条, 无双引号的多个词条视为由 AND 组合
- 例如:
      - page AND content / page content : page 与 content 同时出现在文章中
      - page OR content : page 或 content 之一出现在文章中
      - page content -team : page AND content 的结果，过滤掉含 team 的项
- 注意：单一词条加 - 的情形下， - 会被忽略掉. 如 -team 解释为 team

### 过滤器
有些元数据(metadata)可以用来缩小搜索的范围:
- titleText:space AND team : 搜索 team , 但是只要标题中出现 space 的结果
- labelText:kb-how-to-article : 所有带标签 kb-how-to-article 的项目

实际使用中，左下角图形化的过滤器更有效一些

### 分组运算符(括号)
几乎所有有效的查询语句，都可以使用 ( ) 分组，起到消歧义的作用 :
- (o?tag* OR month -past) AND blog
- o?tag* OR (month -past AND blog)


### 限制
Confluence的搜索功能是基于[Lucene](https://lucene.apache.org/)开发的，不是简单的全文匹配，能够适应海量文档的要求。然而这也意味着查询语言受到了一些限制，比如一些常见词语("the", "and", "or")被忽略了，通配符不能在开头等等。
举例:
* a dog 等于 dog , the ship 等于 ship
* `*`anything, the, and 均不能返回任何结果

如果有其他疑问，请参考官方[文档](https://confluence.atlassian.com/doc/confluence-search-syntax-158720.html)



## 惯例与礼节

### 协作
维基的精神是共享知识，所以如果您被其他用户邀请去编辑一篇文章，请不吝赐教
### 客观
维基上的内容应该是尽可能接近事实真相，所以行文方式宜客观
### 引用
借用他人的观点，应该注意引用[出处](https://creativecommons.org/licenses/by/2.0/)
