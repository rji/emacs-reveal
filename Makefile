all: submodule install publish default
.PHONY: all

default: publish

submodule:
	git submodule sync --recursive
	git submodule update --init --recursive
	
install:
	emacs --batch --load emacs-reveal/install.el --funcall install

publish:
	emacs --batch --load elisp/publish.el --funcall org-publish-all

